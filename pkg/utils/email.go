package utils

import (
	"log"
	"time"

	gomail "gopkg.in/mail.v2"
)

// SendEmail sends an email
func SendEmail(to []string, subject, body string, file string) error {
	m := gomail.NewMessage()
	// Set E-Mail sender
	m.SetHeader("From", "facturacion-oce@central-mx.com")

	// Set E-Mail receivers
	//setheader to array
	m.SetHeader("To", to...)

	// Set E-Mail subject
	m.SetHeader("Subject", subject)

	// Set E-Mail body. You can set plain text or html with text/html
	m.SetBody("text/plain", "Correo de API Ventum")

	// Set E-Mail attachment
	if file != "" {
		m.Attach(file)
	}

	// Settings for SMTP server
	d := gomail.NewDialer("smtp.office365.com", 587, "facturacion-oce@central-mx.com", "1t3gr4d0r2020*")

	// This is only needed when SSL/TLS certificate is not valid on server.
	// In production this should be set to false.
	//d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	// Now send E-Mail
	if err := d.DialAndSend(m); err != nil {
		log.Println(err)
		panic(err)
	}

	return nil
}

func SendLogDay(t time.Time) {
	dt := time.Now()
	Hora := dt.Format("15:04:05")

	if Hora >= "07:00:00" && Hora <= "08:59:59" {
		correos := []string{"lfernando@integrador-technology.mx", "omar@integrador-technology.mx"}
		SendEmail(correos, "Notificación Api Ventum", "Estatus de API Ventum de robot extractor ventum", LogFileNameLastDay())
	}
}
