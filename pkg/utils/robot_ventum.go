package utils

import (
	"bufio"
	"database/sql"
	"fmt"
	"log"
	"os"
	"robot-monitoreo/databases"
	"robot-monitoreo/models"
	"time"

	"gopkg.in/ini.v1"
)

func RobotVentum(ticker time.Time) {
	fmt.Println("Robot Ventum corrio: ", ticker)
	dt := time.Now()
	Hora := dt.Format("15:04:05")

	if Hora >= "05:00:00" && Hora <= "06:59:59" {
		t := time.Now()
		log.Println("Robot Ventum inicia: ")
		fecha2 := t.Add(365*24 - (24 * time.Hour))
		fecha3 := fecha2.Format("2006/01/02")

		var estacionamientos []string

		file, err := os.Open("lista.txt")
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			estacionamientos = append(estacionamientos, scanner.Text())
		}

		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}

		ArrLen := len(estacionamientos)

		cfg, err := ini.Load("config.ini")
		if err != nil {
			fmt.Printf("Fail to read file: %v", err)
			os.Exit(1)
		}

		log.Println("Conectado a la base destino")

		for f := 0; f < ArrLen; f++ {
			no_est := estacionamientos[f]
			// Lectura de valores, pueden ser representados como vacios
			DB_IP := cfg.Section(no_est).Key("DB_IP").String()
			DB_NAME := cfg.Section(no_est).Key("DB_NAME").String()
			DB_USER := cfg.Section(no_est).Key("DB_USER").String()
			DB_PASSWORD := cfg.Section(no_est).Key("DB_PASSWORD").String()

			Conexion := DB_USER + ":" + DB_PASSWORD + "@tcp" + "(" + DB_IP + ":3306)/" + DB_NAME
			go InsertMasivo(fecha3, Conexion, DB_IP)

		}

		time.Sleep(time.Minute * 1)
		defer elapsed("Robot")()
		log.Println("Finalizando robot ventum.... ")
		time.Sleep(time.Second * 5)
	}
} //termina main

//funcion a estacionamiento
func InsertMasivo(fecha_consulta string, conexion string, IP string) {

	const (
		USE_MYMYSQL = false
	)

	driver := ""
	connstr := ""
	if USE_MYMYSQL {
		driver = "mymysql"
		connstr = conexion
		defer Recuperacion("producción")
	} else {
		driver = "mysql"
		connstr = conexion
		defer Recuperacion("Produccion")
	}

	//estacionamiento
	db, err := sql.Open(driver, connstr)

	defer Recuperacion(IP)
	if err != nil {
		log.Printf("Error al conectar a la IP: " + IP)
		log.Print(err)
	}

	db.SetConnMaxLifetime(0)
	db.SetMaxIdleConns(0)
	db.SetMaxOpenConns(0)

	defer db.Close()

	log.Println("Conectado a la base estacionamiento " + IP + " usando el driver: " + driver)

	var count int

	error := db.QueryRow("SELECT COUNT(*) FROM ecentral.ope_operacion AS oope JOIN ecentral.ope_pago AS opag ON (oope.ID = opag.ID_OPERACION) WHERE DATE(oope.FECHA) = ? AND oope.folio IS NOT NULL", fecha_consulta).Scan(&count)
	switch {
	case error != nil:
		log.Println("Error al consultar a la IP: "+IP+": ", error)
		defer Recuperacion(IP)
		log.Print(err)
		panic(err.Error())
	default:
		log.Printf("Cantidad de registros de "+IP+": %v\n", count)
	}

	consulta, err := db.Query("SELECT oope.FECHA, oope.folio, opag.ID_ESTACIONAMIENTO, opag.monto_por_cobrar_local FROM ecentral.ope_operacion AS oope JOIN ecentral.ope_pago AS opag ON (oope.ID = opag.ID_OPERACION) WHERE DATE(oope.FECHA) = ? AND oope.folio IS NOT NULL", fecha_consulta)
	defer Recuperacion(IP)
	if err != nil {
		log.Print(err)
		panic(err.Error())
	}

	log.Println("Comienza insert estacionamiento " + IP)
	var U1 []models.Robot = []models.Robot{}
	for consulta.Next() {
		var Fecha, folio, id_estacionamiento, monto string

		defer Recuperacion(IP)
		if err := consulta.Scan(&Fecha, &folio, &id_estacionamiento, &monto); err != nil {
			log.Fatal(err.Error())
		} //termina err

		defer Recuperacion(IP)
		if err != nil {
			log.Fatal(err)
		}

		if folio == "" {
			continue
		} else {
			// U1 := &models.Robot{Fecha: Fecha, Folio: folio, Monto: monto, Park: id_estacionamiento}
			// databases.Database.Create(U1)
			U1 = append(U1, models.Robot{Fecha: Fecha, Folio: folio, Monto: monto, Park: id_estacionamiento})
		}

		defer Recuperacion(IP)
		if err != nil {
			log.Fatal(err)
		}

	} //termina for

	log.Printf("Cantidad de registros de "+IP+": %v\n", len(U1))

	databases.Database.CreateInBatches(U1, len(U1))

} //termina funcion insert

//funcion de cronometro
func elapsed(what string) func() {
	start := time.Now()
	return func() {
		log.Printf("%s Finalizo inserción: %v\n", what, time.Since(start))
	}
}

func Recuperacion(IP string) {
	recuperado := recover()
	if recuperado != nil {
		log.Println("Recuperación de: ", IP, recuperado)
	}
}
