package utils

import (
	"fmt"
	"log"
	"os"
	"time"
)

func CreateLogFile(ticker time.Time) {
	fmt.Println("Create log corrio: ", ticker)

	//create logs folder if not exists and create log file
	fromDate := time.Now()
	year, month, day := fromDate.Date()

	DirectoryName := fmt.Sprintf("logs/%d-%d", year, month)

	if _, err := os.Stat("logs/" + DirectoryName); os.IsNotExist(err) {
		os.MkdirAll(DirectoryName, 0755)
	}

	logFile := fmt.Sprintf(DirectoryName+"/%d-%d-%d.log", year, month, day)

	if _, err := os.Stat(logFile); os.IsNotExist(err) {
		os.Create(logFile)
	}
}

func LogFileNameLastDay() string {
	fromDate := time.Now()
	yesterday := fromDate.AddDate(0, 0, -1)
	year, month, day := yesterday.Date()
	DirectoryName := fmt.Sprintf("logs/%d-%d", year, month)
	logFile := fmt.Sprintf(DirectoryName+"/%d-%d-%d.log", year, month, day)
	return logFile
}

func LogFileName() string {
	fromDate := time.Now()
	year, month, day := fromDate.Date()
	DirectoryName := fmt.Sprintf("logs/%d-%d", year, month)
	logFile := fmt.Sprintf(DirectoryName+"/%d-%d-%v.log", year, month, day)
	return logFile
}

func DefineLogFile() *os.File {
	file, err := os.OpenFile(LogFileName(), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	//Esta linea ayuda a agregar los logs nativo de go a los de fiber
	log.SetOutput(file)
	return file
}
