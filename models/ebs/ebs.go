package ebs

type IdFactura struct {
	UUID string `json:"uuid"`
}

type Uuids struct {
	Uuids []IdFactura `json:"valoruuid"`
}
