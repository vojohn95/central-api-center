package ebs

type PublicoGeneral struct {
	FOLIO                int     `json:"folio"`
	CLIENTE              int     `json:"cliente"`
	FUENTE               string  `json:"fuente"`
	UUID_SAT             string  `json:"uuid_sat"`
	TOTAL_DE_LA_FACTURA  float64 `json:"total_de_la_factura"`
	SUBTOTAL             float64 `json:"subtotal"`
	IMPUESTO             float64 `json:"impuesto"`
	ENTIDAD_LEGAL        string  `json:"entidad_legal"`
	RFC_EMISOR           string  `json:"rfc_emisor"`
	SERIE                string  `json:"serie"`
	TIPO_COMPROBANTE     string  `json:"tipo_comprobante"`
	FECHA_EMISION        string  `json:"fecha_emision"`
	FECHA_CONTABLE       string  `json:"fecha_contable"`
	RECEPTOR             string  `json:"receptor"`
	RECEPTOR_RFC         string  `json:"receptor_rfc"`
	PROYECTO             string  `json:"proyecto"`
	DESCRIPCION_PROYECTO string  `json:"descripcion_proyecto"`
}

type ValuesFactura struct {
	ValuesFactura []PublicoGeneral `json:"valores"`
}
