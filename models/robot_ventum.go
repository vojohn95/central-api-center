package models

import "gorm.io/gorm"

type Robot struct {
	gorm.Model
	Id    int    `json:"id"`
	Monto string `json:"monto"`
	Folio string `json:"folio"`
	Fecha string `json:"fecha"`
	Park  string `json:"park"`
}

type RobotVentum struct {
	slicerobot []Robot
}
