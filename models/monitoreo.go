package models

import "gorm.io/gorm"

type Monitoreo struct {
	gorm.Model
	Mensaje string `json:"mensaje"`
	Tiempo  string `json:"tiempo"`
}
