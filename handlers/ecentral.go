package handlers

import (
	"robot-monitoreo/databases"
	"robot-monitoreo/models"

	"github.com/gofiber/fiber/v2"
)

func GetPublicoEnGeneral(c *fiber.Ctx) error {
	var dogs []models.Dog

	databases.Database.Find(&dogs)
	return c.Status(fiber.StatusOK).JSON(dogs)
}
