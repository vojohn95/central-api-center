package ebs

import (
	"fmt"
	"robot-monitoreo/databases"
	"robot-monitoreo/models/ebs"

	"github.com/gofiber/fiber/v2"
)

func GetTest(c *fiber.Ctx) error {
	rows, err := databases.EBSConnect().Query("select uuid from id_factura")
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(err)
	}
	defer rows.Close()

	result := []ebs.Uuids{}

	for rows.Next() {
		uuidConsulta := ebs.IdFactura{}

		if err := rows.Scan(&uuidConsulta.UUID); err != nil {
			return err // Exit if we get an error
		}
	
		// Append IdFactura to Uuids
		result = append(result, ebs.Uuids{Uuids: []ebs.IdFactura{uuidConsulta}})
		//result.Uuids = append(result.Employees, employee)
	}

	return c.Status(fiber.StatusOK).JSON(result)
}

func GetPublicoEnGeneral(c *fiber.Ctx) error {
	QueryString := `SELECT distinct(RA.TRX_NUMBER)               "FOLIO",
	hc.ACCOUNT_NUMBER                    "CLIENTE",
	--rl.LINE_NUMBER                        "NUMERO ITEM",
	batc.NAME                            "FUENTE",
	ra.ATTRIBUTE12 "UUID-SAT",
	--ra.*,
	aps.amount_due_original              "TOTAL DE LA FACTURA",
	aps.AMOUNT_LINE_ITEMS_ORIGINAL      "SUBTOTAL",
	aps.TAX_ORIGINAL                     "IMPUESTO",
	ent.name                             "ENTIDAD_ LEGAL",
	reg.REGISTRATION_NUMBER              "RFC EMISOR",
	batc.ATTRIBUTE1                      "SERIE",
	decode(rt.TYPE,'INV','I','CM','E',rt.TYPE) "TIPO COMPROBANTE",
	--ra.ATTRIBUTE11                       "FORMA DE PAGO",
	--ra.ATTRIBUTE10                       "METODO DE PAGO",
	TO_CHAR(ra.CREATION_DATE ,'YYYY/MM/DD') "FECHA_EMISION",
	ra.TRX_DATE                             "FECHA_CONTABLE",
	--inv.SEGMENT1                                     "CLAVE PRODUCTO EBS",
	--inv.SEGMENT2                                     "CLAVE PRODUCTO SAT",
	--inv.SEGMENT3                                     "DESCRIPCION",
	hp.party_name                                    "RECEPTOR",
	hp.JGZZ_FISCAL_CODE                              "RECEPTOR RFC",
	--ra.ATTRIBUTE6                                    "USO CFDI",
	--ra.COMMENTS                                      "COMENTARIOS",
	rt.NAME                                          "PROYECTO",
	rt.DESCRIPTION                                   "DESCRIPCION PROYECTO"
	
FROM ra_customer_trx_all            ra,
ra_customer_trx_lines_all           rl,
ar_payment_schedules_all            aps,
ra_cust_trx_types_all               rt,
hz_cust_accounts                    hc,
hz_parties                          hp,
hz_cust_acct_sites_all              hcasa_bill,
hz_cust_site_uses_all               hcsua_bill,
hz_party_sites                      hps_bill,
RA_BATCH_SOURCES_all                batc,
MTL_SYSTEM_ITEMS_B                  inv,
ra_cust_trx_line_gl_dist_all        rct,
xle_registrations                   reg,
xle_entity_profiles                 ent
WHERE 1 = 1
AND ra.customer_trx_id = aps.customer_trx_id
AND ra.customer_trx_id = rl.customer_trx_id
AND ra.org_id = aps.org_id
AND rct.customer_trx_id = aps.customer_trx_id
AND rct.customer_trx_id = ra.customer_trx_id
and batc.BATCH_SOURCE_ID = ra.BATCH_SOURCE_ID
AND rl.INVENTORY_ITEM_ID = inv.INVENTORY_ITEM_ID
AND rl.INVENTORY_ITEM_ID = inv.INVENTORY_ITEM_ID
AND ra.complete_flag = 'Y'
AND ra.cust_trx_type_id = rt.cust_trx_type_id
AND ra.bill_to_customer_id = hc.cust_account_id
--AND hc.status = 'A'
AND hp.party_id = hc.party_id
AND hcasa_bill.cust_account_id = ra.bill_to_customer_id
AND hcasa_bill.cust_acct_site_id = hcsua_bill.cust_acct_site_id
AND hcsua_bill.site_use_code = 'BILL_TO'
AND hcsua_bill.site_use_id = ra.bill_to_site_use_id
AND hps_bill.party_site_id = hcasa_bill.party_site_id
AND hcasa_bill.status = 'A'
AND hcsua_bill.status = 'A'
AND RECEIPT_REQUIRED_FLAG = 'Y'
and rt.TYPE = 'INV'
and ent.LEGAL_ENTITY_ID  = ra.LEGAL_ENTITY_ID
AND ent.LEGAL_ENTITY_ID  = reg.source_id
--AND ra.GLOBAL_ATTRIBUTE30 IS NULL
--AND batc.ATTRIBUTE1 IS NULL
AND batc.NAME IN ('MANUAL_INGRESOS','E_CENTRAL')
AND aps.TAX_ORIGINAL > 0
AND TO_CHAR(ra.CREATION_DATE ,'YYYY/MM/DD') BETWEEN '2022/07/06' AND '2022/07/07'
ORDER BY  FECHA_EMISION desc, folio desc`

	rows, err := databases.EBSConnect().Query(QueryString)

	if err != nil {
		fmt.Println(err)
	}
	defer rows.Close()

	result := []ebs.ValuesFactura{}

	for rows.Next() {
		c := ebs.PublicoGeneral{}

		if err := rows.Scan(&c.FOLIO, &c.CLIENTE, &c.FUENTE, &c.UUID_SAT, &c.TOTAL_DE_LA_FACTURA, &c.SUBTOTAL, &c.IMPUESTO, &c.ENTIDAD_LEGAL, &c.RFC_EMISOR, &c.SERIE, &c.TIPO_COMPROBANTE, &c.FECHA_EMISION, &c.FECHA_CONTABLE, &c.RECEPTOR, &c.RECEPTOR_RFC, &c.PROYECTO, &c.DESCRIPCION_PROYECTO); err != nil {
			return err // Exit if we get an error
		}

		// Append PublicoGeneral to ValuesFactura
		result = append(result, ebs.ValuesFactura{ValuesFactura: []ebs.PublicoGeneral{c}})
	}

	return c.Status(fiber.StatusOK).JSON(result)
}
