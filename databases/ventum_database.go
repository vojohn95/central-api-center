package databases

import (
	"database/sql"
	"fmt"
	"log"
)

// Database instance
var DBVentum *sql.DB

func ConnectVentum(conexion string, IP string) *sql.DB {
	const (
		USE_MYMYSQL = false // En caso de no funcionar un driver utiliza otro de mysql :3
	)

	driver := ""
	connstr := ""
	if USE_MYMYSQL {
		driver = "mymysql"
		connstr = conexion
		defer Recuperacion("producción")
	} else {
		driver = "mysql"
		connstr = conexion
		defer Recuperacion("Produccion")
	}

	//estacionamiento
	db, err := sql.Open(driver, connstr)

	defer Recuperacion(IP)
	if err != nil {
		fmt.Printf("Error al conectar a la IP: " + IP)
		log.Print(err)
	}

	db.SetConnMaxLifetime(0)
	db.SetMaxIdleConns(0)
	db.SetMaxOpenConns(0)

	//defer db.Close()

	fmt.Println("Conectado a la base estacionamiento " + IP + " usando el driver: " + driver)

	return db
}

func Recuperacion(IP string) {
	recuperado := recover()
	if recuperado != nil {
		fmt.Println("Recuperación de: ", IP, recuperado)
	}
}
