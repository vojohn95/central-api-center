package databases

import (
	"database/sql"
	"fmt"
	"robot-monitoreo/config"

	_ "github.com/godror/godror"
)

// Database instance
var dbEBS *sql.DB

func EBSConnect() *sql.DB {
	// Get db pool object
	connectString := "oracle://" + config.GetEnvValue("EBSCentralUser") + ":" + config.GetEnvValue("EBSCentralPassword") + "@" + config.GetEnvValue("EBSCentralHost") + ":" + config.GetEnvValue("EBSCentralPort") + "/" + config.GetEnvValue("EBSCentralDBName")
	dbEBS, err := sql.Open("godror", connectString)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("EBS Connected")
	//defer dbEBS.Close()

	return dbEBS

}
