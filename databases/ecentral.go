package databases

import (
	"robot-monitoreo/config"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DatabaseEcentral *gorm.DB

var DATABASEEcentral_URI string = config.GetEnvValue("ECentralUser") + ":" + config.GetEnvValue("ECentralPassword") + "@tcp(" + config.GetEnvValue("ECentralHost") + ":" + config.GetEnvValue("ECentralPort") + ")/" + config.GetEnvValue("ECentralDBName") + "?charset=utf8mb4&parseTime=True&loc=Local"

func EConnect() error {
	var err error
	switch config.GetEnvValue("ECentralConnection") {
	case "mysql":
		DatabaseEcentral, err = gorm.Open(mysql.Open(DATABASEEcentral_URI), &gorm.Config{
			SkipDefaultTransaction: true,
			PrepareStmt:            true,
		})
	case "postgres":
		// Database, err = gorm.Open(postgres.Open(DATABASE_URI), &gorm.Config{
		// 	SkipDefaultTransaction: true,
		// 	PrepareStmt:            true,
		// })
	case "sqlite":
		// Database, err = gorm.Open(sqlite.Open(DATABASE_URI), &gorm.Config{
		// 	SkipDefaultTransaction: true,
		// 	PrepareStmt:            true,
		// })
	}

	if err != nil {
		panic(err)
	}

	//DatabaseEcentral.Logger = logger.Default.LogMode(logger.Info)

	//DatabaseEcentral.AutoMigrate(&models.Dog{})

	return nil
}
