package middlewares

import (
	"robot-monitoreo/pkg/utils"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
)

func Params(a *fiber.App) {

	file := utils.DefineLogFile()

	a.Use(
		// Add CORS to each route.
		cors.New(),
		// Add simple logger.
		logger.New(
			logger.Config{
				Output: file,
			},
		),
		//add recover option.
		recover.New(),
		//add compress option.
		compress.New(
			compress.Config{
				Level: compress.LevelBestSpeed,
			},
		),
	)

}
