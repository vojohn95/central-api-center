package main

import (
	"os"
	"robot-monitoreo/config"
	"robot-monitoreo/databases"
	"robot-monitoreo/middlewares"
	"robot-monitoreo/pkg/utils"
	"robot-monitoreo/routes"
	"time"

	"github.com/gofiber/fiber/v2"
)

func main() {
	//logs
	utils.CreateLogFile(time.Now())
	go utils.DoEvery(time.Minute*5, utils.CreateLogFile)
	// Define Fiber config.
	config := config.FiberConfig()

	//start fiber
	app := fiber.New(config)

	// Middlewares.
	middlewares.Params(app)

	//connect database
	databases.Connect()
	//get logs ventum
	go utils.DoEvery(time.Minute*1, utils.ExtractLogs)
	//Robot extractor ventum
	go utils.DoEvery(time.Minute*120, utils.RobotVentum)
	go utils.DoEvery(time.Minute*120, utils.SendLogDay)

	routes.SetupRoutes(app)
	routes.NotFoundRoute(app) // Register route for 404 Error.

	// Start server (with or without graceful shutdown).
	if os.Getenv("STAGE_STATUS") == "dev" {
		utils.StartServer(app)
	} else {
		utils.StartServerWithGracefulShutdown(app)
	}

}
